import React, {useState} from 'react';
import './SplitForm.css';

const SplitForm = () => {

    const [splitInfo, setsplitInfo] = useState({
        quantity : 0,
        sumOfOrder: 0,
        tips: 0,
        delivery: 0,
        total: 0,
        eachBill:0,
    })

    const onInputChange = e =>{
        const {value, name} = e.target;
        setsplitInfo(prev =>({
            ...prev,
            [name]: value
        }))
    }

    const makeCount = ()=>{
        const percent = splitInfo.tips/ 100 *splitInfo.sumOfOrder;
        const total = parseInt(splitInfo.sumOfOrder)+percent+ parseInt(splitInfo.delivery);

        setsplitInfo( prev =>({
            ...prev,
            total: total,
            eachBill: Math.floor(total/splitInfo.quantity),
        }))
    }

    return (
        <div className='split-form'>
            <form>
                <label>Кол-во человек:
                <input
                    type='number'
                    value={splitInfo.quantity}
                    name='quantity'
                    onChange={onInputChange}
                /> <span>чел</span></label>

                <label>Сумма заказа:
                <input
                    type='number'
                    value={splitInfo.sumOfOrder}
                    name='sumOfOrder'
                    onChange={onInputChange}
                /> <span>сом</span> </label>

                <label>Процент чаевых:
                <input
                    type='number'
                    value={splitInfo.tips}
                    name='tips'
                    onChange={onInputChange}
                /> <span>%</span></label>

                <label>Доставка:
                <input
                    type='number'
                    value={splitInfo.delivery}
                    name='delivery'
                    onChange={onInputChange}
                /> <span>сом</span></label>

                <button type='button' className='count-btn' onClick={makeCount}>Рассчитать</button>
            </form>
            <p><i>Общая сумма: </i> {splitInfo.total} c</p>
            <p><i>Кол-во человек: </i> {splitInfo.quantity} чел</p>
            <p><i>Каждый платит по: </i> {splitInfo.eachBill} с</p>
        </div>
    );
};

export default SplitForm;