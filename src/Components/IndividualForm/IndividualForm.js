import React, {useState} from 'react';
import {nanoid} from "nanoid";
import './Individual.css';

const IndividualForm = () => {

    const [people, setPeople] = useState([]);

    const [additionalExpence, setAdditionalExpence] = useState({
        tips: 0,
        delivery: 0,
        total: 0,
        eachBill: 0,
    })



    const addPerson = ()=>{
        setPeople(people =>[...people,{name: '', price: '', id: nanoid()}])
    }

    const onInputChange = e =>{
        const {value, name} = e.target;
        setAdditionalExpence(prev =>({
            ...prev,
            [name]: value
        }));
    }

    const changePersonField = (id,name,value)=>{
        setPeople(people =>{
            return people.map(person=>{
                if(person.id === id){
                    return {...person, [name]: value}
                }
                return person;
            });
        })
    };

    const removePerson = id =>{
        setPeople(people.filter(p=>p.id !== id));
    }

    return (
        <div className='individualForm'>
            {people.map(person =>(
                <div key={person.id} className='person'>
                    <input
                        type='text'
                        placeholder='Имя'
                        value={person.name}
                        autoComplete='off'
                        name='name'
                        onChange={(e)=>changePersonField(person.id,'name', e.target.value,)}
                    />
                    <input
                        type='number'
                        placeholder='Сумма'
                        name='sum'
                        onChange={(e)=>changePersonField(person.id,'sum', e.target.value,)}
                    />
                    <button type='button' className='remove-btn' onClick={()=>removePerson(person.id)} >Удалить</button>
                </div>
            ))}
            <button type='button' onClick={addPerson} className='add-btn'>Добавить человека</button>
            <div className='tips'>
                <label>
                    Процент чаевых:
                    <input
                    type='number'
                    name='tips'
                    value={additionalExpence.tips}
                    onChange={onInputChange}
                     /> <span>%</span>
                </label>
                <label>
                    Доставка:
                    <input
                        type='number'
                        name='delivery'
                        value={additionalExpence.delivery}
                        onChange={onInputChange}
                    /> <span>c</span>
                </label>
                <button type='button' className='count-btn'>Расчитать</button>
            </div>
        </div>
    );
};

export default IndividualForm;