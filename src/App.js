import './App.css';
import {useState} from "react";
import SplitForm from "./Components/SplitForm/SplitForm";
import IndividualForm from "./Components/IndividualForm/IndividualForm";

const App = () => {

    const [mode, setMode]=useState('split');

    const onRadioChange = e =>{
        setMode(e.target.value);
    }

  return (
      <div className="App">
          <div className='radioInputs'>
              <label>
                  <input
                      type='radio'
                      name='mode'
                      value='split'
                      checked={mode === 'split'}
                      onChange={onRadioChange}
                  />{' '}
                  Поровну между всеми участниками
              </label>
              <label>
                  <input
                      type='radio'
                      name='mode'
                      value='individual'
                      checked={mode === 'individual'}
                      onChange={onRadioChange}
                  /> {' '}
                  Каждому индивидуально
              </label>
          </div>
          {mode === 'split' ? (
              <SplitForm/>
          ) : (
              <IndividualForm/>
          )}
      </div>
  );
};

export default App;
